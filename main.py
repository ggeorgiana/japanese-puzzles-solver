import os
import re
import subprocess

from flask import Flask, render_template, request

app = Flask(__name__)


@app.route("/")
@app.route("/index.html")
def index():
    pwd = subprocess.run("pwd", stdout=subprocess.PIPE).stdout.decode("utf-8")
    print(f"PWD: {pwd}")

    input_files = [f for f in os.listdir(os.path.join("./static/input"))]
    return render_template(
        "index.html", the_title="Japanese puzzles", input_files=input_files
    )


@app.route("/")
@app.route("/model.html")
def model():
    pwd = subprocess.run("pwd", stdout=subprocess.PIPE).stdout.decode("utf-8")
    print(f"PWD: {pwd}")

    input_files = [f for f in os.listdir(os.path.join("./static/input"))]
    return render_template(
        "model.html", the_title="Japanese puzzles", input_files=input_files
    )


@app.route("/run", methods=["GET", "POST"])
def mace4_or_prover9():
    input_files = [f for f in os.listdir(os.path.join("./static/input"))]

    # it gets the selected input file
    input_file = request.form.get("input_files_dropdown")

    # builds the full path of the file
    input_file_path = os.path.join(f"./static/input/{input_file}")

    if input_file:
        file_content = read_from_file(input_file_path)
        output = ""
        if request.form["action"] == "Mace4":
            cmd = ["mace4", "-t 5", "-f", input_file_path]
            result = subprocess.run(cmd, stdout=subprocess.PIPE)

            output = result.stdout.decode("utf-8")
            print(output.strip())
        elif request.form["action"] == "Prover9":
            cmd = ["prover9", "-t 5", "-f", input_file_path]
            result = subprocess.run(cmd, stdout=subprocess.PIPE)

            output = result.stdout.decode("utf-8")
            print(output.strip())
    else:
        return index()

    return render_template(
        "index.html",
        the_title="Japanese puzzles",
        result=output,
        input_file=file_content,
        input_files=input_files,
    )


def read_from_file(input_file: str) -> str:
    with open(input_file, "r") as file:
        data = file.read()
    return data


@app.route("/solve", methods=["GET", "POST"])
def solve():
    input_files = [f for f in os.listdir(os.path.join("./static/input"))]

    # it gets the selected input file
    input_file = request.form.get("input_files_dropdown")

    # builds the full path of the file
    input_file_path = os.path.join(f"./static/input/{input_file}")

    if input_file:
        cmd = ["mace4", "-f", input_file_path]
        result = subprocess.run(cmd, stdout=subprocess.PIPE)

        output = result.stdout.decode("utf-8")
        model = re.search("f\(_,_\), (\[[0-9,\s]+\])\)", output, re.MULTILINE).group(1)
        print(model.strip())
    else:
        return index()

    return render_template(
        "model.html",
        the_title="Japanese puzzles",
        result=model,
        input_files=input_files,
    )


if __name__ == "__main__":
    app.run(debug=True)
